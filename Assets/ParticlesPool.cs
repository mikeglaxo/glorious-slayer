using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticlesPool : Singleton<ParticlesPool>
{
    int POOLSIZE = 20;
    [SerializeField]
    ParticleEntry[] particles;
    Dictionary<ParticleFXType, Queue<ParticleFX>> ParticlesPoolsDictionary;

    // Start is called before the first frame update
    void Start()
    {
        ParticlesPoolsDictionary = new Dictionary<ParticleFXType, Queue<ParticleFX>>();
        for (int j = 0; j < particles.Length; j++)
        {
            Queue<ParticleFX> particlesPool = new Queue<ParticleFX>(POOLSIZE);
            for (int i = 0; i < POOLSIZE; i++)
            {
                ParticleFX particle = Instantiate(particles[j]._prefab);
                particlesPool.Enqueue(particle);
            }
            ParticlesPoolsDictionary.Add(particles[j]._type, particlesPool);
        }
    }

   public ParticleFX SpawnParticle(ParticleFXType type, Vector3 position)
    {
        ParticleFX particle = ParticlesPoolsDictionary[type].Dequeue();
        particle.gameObject.SetActive(true);
        particle.transform.position = position;
        return particle;
    }

    public void DespawnParticleFX(ParticleFX particleFX)
    {
        ParticlesPoolsDictionary[particleFX.Type].Enqueue(particleFX);
        particleFX.gameObject.SetActive(false);
    }

    //public void OnBulletStop(Bullet bullet)
    //{
    //    DespawnBullet(bullet);
    //}
}

[Serializable]
public class ParticleEntry
{
    public ParticleFXType _type;
    public ParticleFX _prefab;
}
public enum ParticleFXType
{
    BLOODPUFF,
    DEMONSPAWNMARK
}