using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Enemy : MonoBehaviour
{
    public Action<Enemy> Dead;
    Vector3 _velocityVector;
    [SerializeField]
    float _speedMaximum;
    [SerializeField]
    int _health = 1;
    Transform _slayerTransform;
    EnemyType _type;
    NavMeshAgent _agent;

    public EnemyType Type { get => _type; set => _type = value; }
    public int Health
    {
        get => _health;
        set
        {
            if (value <= 0)
            {
                value = 0;
                Dead?.Invoke(this);
            }

            _health = value;
        }
    }

    private void Awake()
    {
        _agent = GetComponent<NavMeshAgent>();
    }

    // Start is called before the first frame update
    void Start()
    {
        _slayerTransform = Slayer.Instance.transform;
    }

    // Update is called once per frame
    void Update()
    {
        //transform.position += Vector3.Normalize(_slayerTransform.position - transform.position) * _speedMaximum * Time.deltaTime;
        _agent.destination = Slayer.Instance.transform.position;
    }
}
