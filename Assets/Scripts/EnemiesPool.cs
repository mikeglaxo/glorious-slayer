using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemiesPool : Singleton<EnemiesPool>
{
    [SerializeField]
    EnemyPrefabTypeCouple[] _pools;    
    [SerializeField]
    int _poolSize;
    Dictionary<EnemyType, Queue<Enemy>> EnemiesPoolsDictionary;

    protected override void Awake()
    {
        base.Awake();
        EnemiesPoolsDictionary = new Dictionary<EnemyType, Queue<Enemy>>();
        for (int i = 0; i < _pools.Length; i++)
        {
            EnemyPrefabTypeCouple pool = _pools[i];
            Queue<Enemy> EnemiesQueue = new Queue<Enemy>();
            EnemiesPoolsDictionary.Add(pool.Type, EnemiesQueue);
            for (int j = 0; j < _poolSize; j++)
            {
                Enemy enemy = Instantiate(pool.EnemyPrefab);
                enemy.Dead += OnEnemyDie;
                enemy.Type = pool.Type;
                enemy.gameObject.SetActive(false);
                EnemiesQueue.Enqueue(enemy);
            }
        }
    }

    public Enemy SpawnEnemy(EnemyType enemyType)
    {
        return EnemiesPoolsDictionary[enemyType].Dequeue();
    }

    private void OnEnemyDie(Enemy enemy)
    {
        DespawnEnemy(enemy);
    }

    public void DespawnEnemy(Enemy enemy)
    {
        enemy.gameObject.SetActive(false);
        EnemiesPoolsDictionary[enemy.Type].Enqueue(enemy);
    }
}

[Serializable]
public class EnemyPrefabTypeCouple
{
    public EnemyType Type;
    public Enemy EnemyPrefab;
}

public enum EnemyType
{
    NONE = 0,
    ZOMBIE = 1
}