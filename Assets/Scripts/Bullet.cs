using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    public Action<Bullet> BulletStopped;
    [SerializeField]
    protected float _speed;
    protected Rigidbody _rigidbody;
    [SerializeField]
    protected int _damage;
    [SerializeField]
    protected BulletType _type;
    [SerializeField]
    ParticleFXType _enemyHitFx;
    public BulletType Type { get => _type; set => _type = value; }

    // Start is called before the first frame update
    private void Awake()
    {
        _rigidbody = GetComponent<Rigidbody>();
    }
    void OnEnable()
    {
        _rigidbody.velocity = transform.forward * _speed;
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.CompareTag("Enemy"))
        {
            collision.collider.GetComponent<Enemy>().Health -= _damage;
            ParticlesPool.Instance.SpawnParticle(_enemyHitFx, collision.contacts[0].point);
        }

        BulletStopped?.Invoke(this);
    }
}
