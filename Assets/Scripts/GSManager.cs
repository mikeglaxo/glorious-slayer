using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GSManager : Singleton<GSManager>
{
    const int MAXMONSTERS = 5;
    const float SPAWNDELAY = 2;
    [SerializeField]
    float _roomWidth;
    [SerializeField]
    Transform[] _spawningPoints;
    [SerializeField]
    float _roomLength;
    List<Enemy> _spawnedEnemies = new List<Enemy>();
    float _spawnWait = 0;

    public float RoomWidth { get => _roomWidth; set => _roomWidth = value; }
    public float RoomLength { get => _roomLength; set => _roomLength = value; }

    // Start is called before the first frame update
    void Start()
    {
        for (int i = 0; i < 3; i++)
        {
            RandomSpawn(EnemyType.ZOMBIE);
        }
    }
    // Update is called once per frame

    private void Update()
    {
        if (_spawnedEnemies.Count < 5 && _spawnWait >= SPAWNDELAY)
        {
            _spawnWait = 0;
            RandomSpawn(EnemyType.ZOMBIE);
        }
        else 
        {
            _spawnWait += Time.deltaTime;
        }
    }

    public Enemy GetClosestEnemy(Vector3 position)
    {
        float distance = Mathf.Infinity;
        Enemy closestEnemy = null;
        for (int i = 0; i < _spawnedEnemies.Count; i++)
        {
            Enemy enemy = _spawnedEnemies[i];
            if (Vector3.Distance(enemy.transform.position, position) < distance)
            {
                closestEnemy = enemy;
            }
        }

        return closestEnemy;
    }

    public void RandomSpawn(EnemyType enemyType)
    {
        int randomSPIndex = UnityEngine.Random.Range(0, _spawningPoints.Length);
        Vector3 spawnPosition = _spawningPoints[randomSPIndex].position;
        Enemy enemy = EnemiesPool.Instance.SpawnEnemy(enemyType);
        enemy.Dead += OnEnemyDead;
        enemy.gameObject.SetActive(true);
        _spawnedEnemies.Add(enemy);
        enemy.transform.position = spawnPosition;
    }

    private void OnEnemyDead(Enemy enemy)
    {
        _spawnedEnemies.Remove(enemy);
    }
}
