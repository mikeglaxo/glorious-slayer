using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class Slayer : Singleton<Slayer>
{
    Transform _target;
    Vector3 _horizontalVelocityVector;
    float _verticalSpeed;
    [SerializeField]
    PlayerInput _playerInput;
    [SerializeField]
    float _speedMaximum;
    [SerializeField]
    float _accelerationSmooth = 1f;
    [SerializeField]
    float _jumpSpeed;
    [SerializeField]
    float _gravity;
    [SerializeField]
    Transform _bulletsSpawnPoint;
    [SerializeField]
    float _sprintRechargeTime = 3f;
    bool _isRechargingSprint;
    float _speed = 0;
    Vector2 _inputTurn;

    [SerializeField]
    AudioClip _bounceClip;
    [SerializeField]
    AudioClip _impactClip;
    [SerializeField]
    AudioClip _gunShotClip;
    AudioSource _audioSource;
    bool _isMidAir;
    bool _hasJumped;
    Rigidbody _rigidbody;

    protected override void Awake()
    {
        base.Awake();
        _rigidbody = GetComponent<Rigidbody>();
        _audioSource = GetComponent<AudioSource>();
    }
    // Start is called before the first frame update
    void Start()
    {
        _horizontalVelocityVector = new Vector3(Random.Range(0f, 1f), 0, Random.Range(0f, 1f)).normalized;//Vector3.Normalize(_target.position - transform.position);
        _rigidbody.velocity = _horizontalVelocityVector * _speedMaximum;
    }

    // Update is called once per frame
    void Update()
    {
        if (_hasJumped)
        {
            _verticalSpeed = _jumpSpeed;
            _isMidAir = true;
            _hasJumped = false;
        }

        if (_inputTurn.x != 0.0f || _inputTurn.y != 0.0f)
        {
            var angle = Mathf.Atan2(_inputTurn.x, _inputTurn.y) * Mathf.Rad2Deg;
            Debug.Log("angle " + angle);
            Vector3 slayerRotation = transform.rotation.eulerAngles;
            transform.rotation = Quaternion.Euler(new Vector3(transform.rotation.eulerAngles.x, angle, transform.rotation.eulerAngles.z));
            // Do something with the angle here.
        }
        _speed = Mathf.Lerp(_speed, _speedMaximum, _accelerationSmooth * Time.deltaTime);
        Vector3 velocityVector = _horizontalVelocityVector * _speed + Vector3.up * _verticalSpeed;
        //transform.position += velocityVector * Time.deltaTime;
        _rigidbody.velocity = velocityVector;

        if (_isMidAir)
        {
            _verticalSpeed -= _gravity * Time.deltaTime;
        }


    }

    IEnumerator RechargeSprintCoroutine()
    {
        yield return new WaitForSeconds(_sprintRechargeTime);
        _isRechargingSprint = false;
    }

    public void OnTurn(InputAction.CallbackContext value)
    {
        Vector2 inputTurn = value.ReadValue<Vector2>();
        _inputTurn = inputTurn;
    }

    //public void OnJump(InputAction.CallbackContext value)
    //{
    //    if (value.performed && !_isMidAir)
    //    {
    //        _hasJumped = true;
    //    }
    //}

    public void OnShoot(InputAction.CallbackContext value)
    {
        if (value.started)
        {
            ShootBullet(BulletType.GUN);
        }
    }

    public void OnStrifeLeft(InputAction.CallbackContext value)
    {
        if (value.performed && !_isRechargingSprint)
        {
            _isRechargingSprint = true;
            StartCoroutine(RechargeSprintCoroutine());
            _speed += 5f;
            _horizontalVelocityVector = -transform.right;
        }
    }

    public void OnStrifeRight(InputAction.CallbackContext value)
    {
        if (value.performed && !_isRechargingSprint)
        {
            _isRechargingSprint = true;
            StartCoroutine(RechargeSprintCoroutine());
            _speed += 5f;
            _horizontalVelocityVector = transform.right;
        }
    }

    void ShootBullet(BulletType type)
    {
        Bullet bullet = BulletsPool.Instance.SpawnBullet(type);
        bullet.transform.position = _bulletsSpawnPoint.position;
        bullet.transform.rotation = _bulletsSpawnPoint.rotation;
        bullet.gameObject.SetActive(true);
        _audioSource.PlayOneShot(_gunShotClip);
        //bullet.
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.CompareTag("Wall"))
        {
            _horizontalVelocityVector = Vector3.Reflect(_horizontalVelocityVector, collision.contacts[0].normal);
            _rigidbody.velocity = _horizontalVelocityVector;
            _audioSource.PlayOneShot(_impactClip);
        }

        if (collision.collider.CompareTag("Bouncer"))
        {
            Bouncer bouncer = collision.collider.GetComponentInParent<Bouncer>();
            if (bouncer.IsInibited)
            {
                _horizontalVelocityVector = Vector3.Reflect(_horizontalVelocityVector, collision.contacts[0].normal);
                _audioSource.PlayOneShot(_impactClip);
            }
            else
            {
                _speed += bouncer.SpeedBoost;
                _horizontalVelocityVector = Vector3.Reflect(_horizontalVelocityVector, collision.contacts[0].normal);
                bouncer.IsInibited = true;
                _audioSource.PlayOneShot(_bounceClip);
            }
        }

        if (collision.collider.CompareTag("Floor"))
        {
            _isMidAir = false;
            _verticalSpeed = 0;
            transform.position = new Vector3(transform.position.x, 0, transform.position.z);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Enemy"))
        {
            Debug.Log("EnemyHit");
        }
    }
}



public enum BulletType
{
    NONE = 0,
    GUN = 1
}
