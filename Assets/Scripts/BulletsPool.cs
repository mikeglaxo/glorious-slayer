using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletsPool : Singleton<BulletsPool>
{
    int POOLSIZE = 20;
    [SerializeField]
    Bullet _gunBullet;
    Queue<Bullet> _gunBulletsPool = new Queue<Bullet>();
    // Start is called before the first frame update
    void Start()
    {
        for (int i = 0; i < POOLSIZE; i++)
        {
            Bullet bullet = Instantiate(_gunBullet);
            _gunBulletsPool.Enqueue(bullet);
            bullet.BulletStopped += OnBulletStop;
        }
    }

    // Update is called once per frame
    void Update()
    {

    }

    public Bullet SpawnBullet(BulletType type)
    {
        Bullet bullet = null;
        switch (type)
        {
            case BulletType.GUN:
                bullet = _gunBulletsPool.Dequeue();
                break;
        }

        return bullet;
    }

    public void DespawnBullet(Bullet bullet)
    {
        switch (bullet.Type)
        {
            case BulletType.GUN:
                _gunBulletsPool.Enqueue(bullet);
                break;
        }
        bullet.gameObject.SetActive(false);
    }

    public void OnBulletStop(Bullet bullet)
    {
        DespawnBullet(bullet);
    }
}
