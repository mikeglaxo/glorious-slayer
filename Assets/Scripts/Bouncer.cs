using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bouncer : MonoBehaviour
{
    [SerializeField]
    float _speedBoost;
    [SerializeField]
    float _inibitionTime = 0.5f;
    bool _isInibited;
    public float SpeedBoost { get => _speedBoost; set => _speedBoost = value; }
    public bool IsInibited
    {
        get => _isInibited;
        set
        {
            _isInibited = value;
            if (value)
            {
                StartCoroutine(InibitionCoroutine());
            }
        }
    }

    IEnumerator InibitionCoroutine()
    {
        yield return new WaitForSeconds(_inibitionTime);
        _isInibited = false;
    }

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
}
