using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleFX : MonoBehaviour
{
    [SerializeField]
    ParticleFXType _type;
    [SerializeField]
    AudioClip _fXClip;
    [SerializeField]
    float _automaticDeactivationTime = 1f;
    AudioSource _audiosource;
    Coroutine _deactivationCoroutine;


    private void Awake()
    {
        _audiosource = GetComponent<AudioSource>();
    }

    private void OnEnable()
    {
        _audiosource.PlayOneShot(_fXClip);
        _deactivationCoroutine = StartCoroutine(DeactivationCoroutine());
    }

    private void OnDisable()
    {
        if (_deactivationCoroutine != null)
        {
            StopCoroutine(_deactivationCoroutine);
            _deactivationCoroutine = null;
        }
    }

    private IEnumerator DeactivationCoroutine()
    {
        yield return new WaitForSeconds(_automaticDeactivationTime);
        ParticlesPool.Instance.DespawnParticleFX(this);
    }
    public ParticleFXType Type { get => _type; set => _type = value; }
}
